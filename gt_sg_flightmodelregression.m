function data = gt_sg_flightmodelregression(data,diveSubset)
%
% Inputs:
% data = standard data structure from toolbox
% diveSubset (optional) = array of dive numbers to regress on using the
% automatic function. Requires 'length' parameter in sg_calib_constants.m or
% gt_sg_settings.m
%
% If hd_* parameters are 1 x numel(dives) size, each dive will be
% calculated with its own hd_* parameters and the GUI bypassed.
%
% 20 dives enough for regression. Check moving 20 dive window for time
% varying parameters.
%
% w2 vs N
%
%
%
% data = gt_sg_sub_flightvec(data)
%
% Wrapper for the UW flightvec.m routines which calculates, using the
% hydrodynamic flight model and buoyancy, the speed and glide angle of the
% glider. This is a better (and iterative) approximation of flight than the
% glider_slope version.
%
% All credits to original authors at the University of Washington
%
% B.Y.QUESTE Jul 2016
%
HelpInfo = ...
    {'If you want the quick and easy option, just click the button on the right. ->',...
    ' ',...
    'Otherwise, use the first tab to select useful data and regress in the second tab. The two cost functions work better on different things. The one labelled hydro works best on hd_ parameters. The one labelled buoyancy copes best with volmax, abs_compress and therm_expan.',...
    ' ',...
    'Info on visualisation tab...'};

%% Setup settings storage
dives = gt_sg_sub_check_dive_validity(data);

% Data selection
FRS.surface_cutoff = 3;
FRS.apogee_cutoff = 3;
FRS.roll_max = 5;
FRS.pitch_max = 90;
FRS.stall_max = 8;
FRS.motor_trim = 1;
FRS.dives = dives;
FRS.use_regress = [];
FRS.vertaccel_max = 0.02;
FRS.vertvel_min = 0.02;

if isfield(data.gt_sg_settings,'flight_regression_settings')
    fields = fieldnames(data.gt_sg_settings.flight_regression_settings);
    for fstep = 1:numel(fields)
        FRS.(fields{fstep}) = data.gt_sg_settings.flight_regression_settings.(fields{fstep});
    end
end

%% Extract basic variables
cast_dir = arrayfun(@(x) ((x.elaps_t*0)+1).*x.dive,data.eng,'Uni',0); % Dive no
for dstep=1:numel(cast_dir)
    cast_dir{dstep}(1:data.hydrography(dstep).max_pressure_index) = -cast_dir{dstep}(1:data.hydrography(dstep).max_pressure_index); % Sign for direction
end

if isfield(data,'flag')
    good_ind = arrayfun(@(x) ~x.salinity & ~x.conductivity & ~x.temp,data.flag,'Uni',0);
else
    good_ind = arrayfun(@(x) true(size(x.time)),data.hydrography,'Uni',0);
end

% Hull length
HullLength = {'Standard',1.8;...
    'Ogive',2.0;...
    'Deep (N/A)',2.2};
if isfield(data.gt_sg_settings,'length')
    hullLength = data.gt_sg_settings.length;
else
    try
        hullLength = mode([data.log.LENGTH]);
    catch
        hullLength = 1.8;
    end
end
hullLength = find([HullLength{:,2}] == hullLength);
data.gt_sg_settings.length = HullLength{hullLength,2};

% Set default constraint limits
h_regress_para.hd_a.min = 0.001; h_regress_para.hd_a.max = 0.007;
h_regress_para.hd_b.min = 0.004; h_regress_para.hd_b.max = 0.02;
h_regress_para.hd_c.min = 1.0e-6; h_regress_para.hd_c.max = 3.0e-5;
h_regress_para.volmax.min = 5.0e4; h_regress_para.volmax.max = 5.5e4;
h_regress_para.abs_compress.min = 1.0e-06; h_regress_para.abs_compress.max = 3.0e-5;
h_regress_para.therm_expan.min = 5.0e-05; h_regress_para.therm_expan.max = 5.0e-4;

%% Create panel
close all;
if nargin > 1
    h_gui = figure('Visible','off');
    
    if diveSubset == -1 
        hd_array = 0;
        FRS.dives = gt_sg_sub_check_dive_validity(data);
        gt_sg_sub_echo({'Applying hydrodynamic calculations to all dives.'});
        run_flightmodel(data.gt_sg_settings);
        return;
    elseif diveSubset == 0
        diveSubset = dives;
    end
    
    gt_sg_sub_echo({'Running automated flight model regression on dives:',num2str(diveSubset)});
    auto = true;
    FRS.dives = gt_sg_sub_check_dive_validity(data,diveSubset);
else
    h_gui = figure('Visible','off');
    % Set options
    set(h_gui,...
        'MenuBar','none',...
        'Units','normalized',...
        'Position',[0.05 0.05 0.9 0.9]...
        );
    auto = false;
end


%% Populate tabs
h_tabs = uitabgroup(h_gui,'Position',[0 0 1 1]);
h_tab(1) = uitab(h_tabs,'Title','1. Data Selection');
h_tab(2) = uitab(h_tabs,'Title','2. Parameter Regression');
h_tab(3) = uitab(h_tabs,'Title','3. Visualisation');
h_tab(4) = uitab(h_tabs,'Title','4. History');
h_tab(5) = uitab(h_tabs,'Title','5. Help');

%% Tab 1 - Data Selection
uicontrol(h_tab(1),'Style','pushbutton','String','Save settings',...
    'Units','normalized','Position',[0.87 0.9 0.12 0.07],...
    'CallBack',@button_data_save);
uicontrol(h_tab(1),'Style','pushbutton','String','Plot selection',...
    'Units','normalized','Position',[0.74 0.9 0.12 0.07],...
    'CallBack',@button_data_plot);

% Apogee and surface cutoff
uicontrol(h_tab(1),'Style','text','String','Surface cutoff (m)',...
    'Units','normalized','Position',[0.01 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Apogee cutoff (m)',...
    'Units','normalized','Position',[0.01 0.90 0.10 0.03]);
h_data_cutoff_surface = uicontrol(h_tab(1),'Style','edit','String',FRS.surface_cutoff,...
    'Units','normalized','Position',[0.12 0.94 0.06 0.03]);
h_data_cutoff_apogee = uicontrol(h_tab(1),'Style','edit','String',FRS.apogee_cutoff,...
    'Units','normalized','Position',[0.12 0.90 0.06 0.03]);

% Roll, pitch, stall and post motor movement cutoff
uicontrol(h_tab(1),'Style','text','String','Roll max (deg)',...
    'Units','normalized','Position',[0.19 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Pitch max (deg)',...
    'Units','normalized','Position',[0.19 0.90 0.10 0.03]);
h_data_cutoff_rollmax = uicontrol(h_tab(1),'Style','edit','String',FRS.roll_max,...
    'Units','normalized','Position',[0.30 0.94 0.06 0.03]);
h_data_cutoff_pitchmax = uicontrol(h_tab(1),'Style','edit','String',FRS.pitch_max,...
    'Units','normalized','Position',[0.30 0.90 0.06 0.03]);
uicontrol(h_tab(1),'Style','text','String','Stall angle (deg)',...
    'Units','normalized','Position',[0.37 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Motor trim (samples)',...
    'Units','normalized','Position',[0.37 0.90 0.10 0.03]);
h_data_cutoff_stallmax = uicontrol(h_tab(1),'Style','edit','String',FRS.stall_max,...
    'Units','normalized','Position',[0.48 0.94 0.06 0.03]);
h_data_cutoff_motor = uicontrol(h_tab(1),'Style','edit','String',FRS.motor_trim,...
    'Units','normalized','Position',[0.48 0.90 0.06 0.03]);
uicontrol(h_tab(1),'Style','text','String','Min dP/dt (m/s)',...
    'Units','normalized','Position',[0.55 0.94 0.10 0.03]);
uicontrol(h_tab(1),'Style','text','String','Max accel. (m/s^2)',...
    'Units','normalized','Position',[0.55 0.90 0.10 0.03]);
h_data_cutoff_vertvelmin = uicontrol(h_tab(1),'Style','edit','String',FRS.vertvel_min,...
    'Units','normalized','Position',[0.66 0.94 0.06 0.03]);
h_data_cutoff_vertaccelmax = uicontrol(h_tab(1),'Style','edit','String',FRS.vertaccel_max,...
    'Units','normalized','Position',[0.66 0.90 0.06 0.03]);

% Dive Selection
uicontrol(h_tab(1),'Style','text','String','Dive numbers',...
    'Units','normalized','Position',[0.01 0.86 0.10 0.03]);
h_data_list_dives = uicontrol(h_tab(1),'Style','listbox','String',dives,...
    'Units','normalized','Position',[0.12 0.01 0.06 0.88],...
    'Max',10000,'Min',1,'Value',indicesIn(dives,FRS.dives));
uicontrol(h_tab(1),'Style','pushbutton','String','Select all',...
    'Units','normalized','Position',[0.01 0.05 0.10 0.03],...
    'CallBack',@(x,y) set(h_data_list_dives,'Value',1:numel(dives)));
uicontrol(h_tab(1),'Style','pushbutton','String','Select none',...
    'Units','normalized','Position',[0.01 0.01 0.10 0.03],...
    'CallBack',@(x,y) set(h_data_list_dives,'Value',[]));

% Plots
h_data_plot_pitch = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.035 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_pitch,'YLabel'),'String','Pitch angle (deg)')
hold on;
plot(h_data_plot_pitch,[data.hydrography(dives).time],[data.eng(dives).pitchAng],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_pitch_blue = plot(h_data_plot_pitch,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_pitch,'x'); axis(h_data_plot_pitch,'tight');

h_data_plot_roll = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.21 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_roll,'YLabel'),'String','Roll angle (deg)')
hold on;
plot(h_data_plot_roll,[data.hydrography(dives).time],[data.eng(dives).rollAng],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_roll_blue = plot(h_data_plot_roll,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_roll,'x'); axis(h_data_plot_roll,'tight');

h_data_plot_total = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.385 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_total,'YLabel'),'String','dP/dt (m/s)')
hold on;
plot(h_data_plot_total,[data.hydrography(dives).time],[data.flight(dives).glide_vert_spd],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_total_blue = plot(h_data_plot_total,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_total,'x'); axis(h_data_plot_total,'tight');

h_data_plot_accel = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.56 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_accel,'YLabel'),'String','Acceleration (m/s^2)')
hold on;
plot(h_data_plot_accel,[data.hydrography(dives).time],[data.flight(dives).glide_vert_spd],'LineStyle','none','Marker','.','MarkerSize',1,'Color',[0.8 0.8 0.8]);
h_data_plot_accel_blue = plot(h_data_plot_total,[],[],'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_accel,'x'); axis(h_data_plot_accel,'tight');

h_data_plot_vbd = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.2 0.735 0.74 0.14],'Parent',h_tab(1));
set(get(h_data_plot_vbd,'YLabel'),'String','C\_VBD')
hold on;
plot(h_data_plot_vbd,dives,[data.log(dives).C_VBD],'LineStyle','none','Marker','*','Color',[0.8 0.8 0.8]);
h_data_plot_vbd_blue = plot(h_data_plot_vbd,[],[],'LineStyle','none','Marker','*','Color','b'); axis(h_data_plot_vbd,'tight');

% Populate with current selection
func_data_makeplots;

% Callbacks
    function button_data_save(button_data,event_data)
        button_data_plot(button_data,event_data)
        data.gt_sg_settings.flight_regression_settings = FRS;
        set(h_tabs,'SelectedTab',h_tab(2));
        
        % Initialise second tab
        button_regress_regresscheckbox(0,0);
        button_regress_plot_velocities(0,0);
    end

    function button_data_plot(~,~)
        FRS.surface_cutoff = str2double(get(h_data_cutoff_surface,'String'));
        FRS.apogee_cutoff = str2double(get(h_data_cutoff_apogee,'String'));
        FRS.roll_max = str2double(get(h_data_cutoff_rollmax ,'String'));
        FRS.pitch_max = str2double(get(h_data_cutoff_pitchmax,'String'));
        FRS.stall_max = str2double(get(h_data_cutoff_stallmax,'String'));
        FRS.motor_trim = str2double(get(h_data_cutoff_motor,'String'));
        FRS.vertvel_min = str2double(get(h_data_cutoff_vertvelmin,'String'));
        FRS.vertaccel_max = str2double(get(h_data_cutoff_vertaccelmax,'String'));
        FRS.dives = dives(get(h_data_list_dives,'Value'));
        
        func_data_makeplots;
    end

    function func_data_makeplots
        delete(h_data_plot_total_blue);
        delete(h_data_plot_accel_blue);
        delete(h_data_plot_vbd_blue);
        delete(h_data_plot_pitch_blue);
        delete(h_data_plot_roll_blue);
        
        for dstep = 1:numel(data.hydrography);
            FRS.use_regress{dstep} = false(size(data.hydrography(dstep).depth));
            if any(FRS.dives == dstep)
                GC_phase = data.eng(dstep).GC_phase == 6;
                if FRS.motor_trim < 1
                    GC_phase(:) = true;
                elseif FRS.motor_trim > 1;
                    GC_trim = @(gc) [gc - [0 diff(gc)> 0]]; 
                    for istep = 1:ceil(FRS.motor_trim)-1
                        GC_phase = GC_trim(GC_phase);
                    end
                end
                FRS.use_regress{dstep}(...
                    data.hydrography(dstep).depth > FRS.surface_cutoff ...
                    & data.hydrography(dstep).depth < data.hydrography(dstep).depth(data.hydrography(dstep).max_pressure_index)-FRS.apogee_cutoff ...
                    & abs(data.eng(dstep).rollAng) < FRS.roll_max ...
                    & abs(data.eng(dstep).pitchAng) < FRS.pitch_max ...
                    & abs(data.eng(dstep).pitchAng) > FRS.stall_max ...
                    & abs(data.flight(dstep).glide_vert_spd) > FRS.vertvel_min ...
                    & abs(gt_sg_sub_diff({data.flight(dstep).glide_vert_spd,data.hydrography(dstep).time.*24.*60.*60})) < FRS.vertaccel_max ...
                    & GC_phase ...
                    & good_ind{dstep} ...
                    ) = true;                
            end
        end
        
        ind = [FRS.use_regress{dives}];
        time = [data.hydrography(dives).time];
        vert_spd = [data.flight(dives).glide_vert_spd];
        pitchAng = [data.eng(dives).pitchAng];
        rollAng = [data.eng(dives).rollAng];
        accel = gt_sg_sub_diff({[data.flight.glide_vert_spd],[data.hydrography.time]*24*60*60});
        
        uicontrol(h_tab(1),'Style','text','String',{'Number of Points:',num2str(sum(ind)),'of',num2str(numel(time))},...
            'Units','normalized','Position',[0.01 0.4 0.10 0.1],...
            'CallBack',@(x,y) set(h_data_list_dives,'Value',1:numel(dives)));
        
        h_data_plot_pitch_blue = plot(h_data_plot_pitch,time(ind),pitchAng(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_pitch,'x'); axis(h_data_plot_pitch,'tight');
        h_data_plot_roll_blue = plot(h_data_plot_roll,time(ind),rollAng(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_roll,'x'); axis(h_data_plot_roll,'tight');
        h_data_plot_total_blue = plot(h_data_plot_total,time(ind),vert_spd(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_total,'x'); axis(h_data_plot_total,'tight');
        h_data_plot_accel_blue = plot(h_data_plot_accel,time(ind),accel(ind),'LineStyle','none','Marker','.','MarkerSize',1,'Color','b'); datetick(h_data_plot_total,'x'); axis(h_data_plot_total,'tight');
        h_data_plot_vbd_blue = plot(h_data_plot_vbd,FRS.dives,[data.log(FRS.dives).C_VBD],'LineStyle','none','Marker','*','Color','b'); axis(h_data_plot_vbd,'tight');
    end

%% Tab 2 - Parameter Regression
uicontrol(h_tab(2),'Style','pushbutton','String','View results',...
    'Units','normalized','Position',[0.87 0.91 0.12 0.07],...
    'CallBack',@button_regress_plot_velocities);
uicontrol(h_tab(2),'Style','pushbutton','String','Store results',...
    'Units','normalized','Position',[0.87 0.83 0.12 0.07],...
    'CallBack',@button_regress_store);
uicontrol(h_tab(2),'Style','pushbutton','String','Undo',...
    'Units','normalized','Position',[0.87 0.75 0.12 0.07],...
    'CallBack',@button_regress_undo);
uicontrol(h_tab(2),'Style','pushbutton','String','Restore defaults',...
    'Units','normalized','Position',[0.87 0.67 0.12 0.07],...
    'CallBack',@button_regress_reset);

% Panels
h_regress_buoy = uipanel('Parent',h_tab(2),'Title','Buoyancy parameters',...
    'Units','normalized','Position',[0.01 0.01 0.485 .49]);
h_regress_hydro = uipanel('Parent',h_tab(2),'Title','Hydrodynamic parameters',...
    'Units','normalized','Position',[0.01 0.505 0.485 .485]);
h_regress_cost = uibuttongroup('Parent',h_tab(2),'Title','Cost function',...
    'Units','normalized','Position',[0.50 0.505 0.18 .485]);
h_regress_opt = uipanel('Parent',h_tab(2),'Title','Settings',...
    'Units','normalized','Position',[0.685 0.505 0.18 .485]);
h_regress_iter = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.50 0.1 0.21 0.39],'Parent',h_tab(2)); hold on;
h_regress_vis = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.76 0.1 0.21 0.39],'Parent',h_tab(2)); ...
    hold on; ylabel(h_regress_vis,'Depth (m)'); xlabel(h_regress_vis,{'Red - dP/dt, Blue - buoyancy,','Green - up/downwelling (m.s^-^1)'});

% Settings
uicontrol(h_regress_opt,'Style','text','String','Fairing type:',...
    'Units','normalized','Position',[0.01 0.93 0.48 0.04]);
h_regress_opt_length = uicontrol(h_regress_opt,'Style','popup','String',HullLength(:,1),'Value',hullLength,...
    'Min',1,'Max',1,'Units','normalized','Position',[0.51 0.93 0.48 0.04],...
    'CallBack',...
    @button_regress_hullLength);
uicontrol(h_regress_opt,'Style','text','String','Constrain:',...
    'Units','normalized','Position',[0.01 0.82 0.48 0.04]); constrainRegression = {'Off','On'};
h_regress_opt_constrain = uicontrol(h_regress_opt,'Style','popup','String',constrainRegression,'Value',1,...
    'Min',1,'Max',1,'Units','normalized','Position',[0.51 0.82 0.48 0.04],...
    'CallBack',@button_regress_constrain);
uicontrol(h_regress_opt,'Style','text','String','Max iterations: ',...
    'Units','normalized','Position',[0.01 0.71 0.48 0.04]);
h_regress_opt_maxiter = uicontrol(h_regress_opt,'Style','edit','String',100,...
    'Units','normalized','Position',[0.50 0.68 0.48 0.1]);
uicontrol(h_regress_opt,'Style','text','String','Function tolerance:',...
    'Units','normalized','Position',[0.01 0.60 0.48 0.04]);
h_regress_opt_tolfun = uicontrol(h_regress_opt,'Style','edit','String',1.0e-10,...
    'Units','normalized','Position',[0.50 0.57 0.48 0.1]);
h_regress_button_process = uicontrol(h_regress_opt,'Style','pushbutton','String','Regress parameters',...
    'Units','normalized','Position',[0.17 0.41 0.74 0.14],...
    'CallBack',@button_regress_process);
h_regress_button_auto = uicontrol(h_regress_opt,'Style','pushbutton','String','Attempt auto regress',...
    'Units','normalized','Position',[0.17 0.13 0.74 0.14],...
    'CallBack',@button_regress_auto,'Enable','on');
uicontrol(h_regress_opt,'Style','text','String',{'Number of','rangefinding passes:'},...
    'Units','normalized','Position',[0.01 0.01 0.48 0.1]);
h_regress_opt_autopass = uicontrol(h_regress_opt,'Style','edit','String','2',...
    'Units','normalized','Position',[0.50 0.01 0.48 0.1],'Enable','on');

% Cost function options
h_regress_cost_radio(1) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'Minimise mean(W(H2O)) (buoyancy)',...
    'Units','normalized','Position',[0.01 0.9 0.98 0.1]);
h_regress_cost_radio(2) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'Minimise mode(W(H2O)) (convective environments)',...
    'Units','normalized','Position',[0.01 0.7 0.98 0.1]);
h_regress_cost_radio(3) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
    'RMSD of up & down W(H2O) (hydro)',...
    'Units','normalized','Position',[0.01 0.5 0.98 0.1],'Enable','on');
h_regress_cost_radio(4) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
     'dP/dt vs. pitch space minimisation',...
     'Units','normalized','Position',[0.01 0.3 0.98 0.1],'Enable','on');
% h_regress_cost_radio(5) = uicontrol(h_regress_cost,'Style','radiobutton','String',...
%     'Cost fun 1 * cost fun 2 ?? Something nice and overarching?',...
%     'Units','normalized','Position',[0.01 0.1 0.98 0.1],'Enable','off');

% Hydro and buoy panels
h_regress_para.hd_a.panel = uipanel('Parent',h_regress_hydro,'Title','HD_A',...
    'Units','normalized','Position',[0.01 0.04+2*(1-0.06)/3 0.98 (1-0.06)/3]);
h_regress_para.hd_b.panel = uipanel('Parent',h_regress_hydro,'Title','HD_B',...
    'Units','normalized','Position',[0.01 0.03+(1-0.06)/3 0.98 (1-0.06)/3]);
h_regress_para.hd_c.panel = uipanel('Parent',h_regress_hydro,'Title','HD_C',...
    'Units','normalized','Position',[0.01 0.02 0.98 (1-0.06)/3]);
h_regress_para.volmax.panel = uipanel('Parent',h_regress_buoy,'Title','Volmax',...
    'Units','normalized','Position',[0.01 0.04+2*(1-0.06)/3 0.98 (1-0.06)/3]);
h_regress_para.abs_compress.panel = uipanel('Parent',h_regress_buoy,'Title','Absolute compression',...
    'Units','normalized','Position',[0.01 0.03+(1-0.06)/3 0.98 (1-0.06)/3]);
h_regress_para.therm_expan.panel = uipanel('Parent',h_regress_buoy,'Title','Thermal expansion',...
    'Units','normalized','Position',[0.01 0.02 0.98 (1-0.06)/3]);

% Populate parameter panels
parameters = {'hd_a','hd_b','hd_c','volmax','abs_compress','therm_expan'};
for pstep = 1:numel(parameters)
    
    h_regress_para.(parameters{pstep}).regress = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','checkbox','String','Regress?',...
        'Units','normalized','Position',[0.01 0.8 0.28 0.2],...
        'CallBack',@button_regress_regresscheckbox);
    
    h_regress_para.(parameters{pstep}).initial = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','edit','String',data.gt_sg_settings.(parameters{pstep}),...
        'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')}, 'Units','normalized','Position',[0.51 0.8 0.48 0.2]);
    uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','text','String','Initial guess: ',...
        'Units','normalized','Position',[0.30 0.8 0.2 0.2]);
    
    h_regress_para.(parameters{pstep}).minval = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','edit','String',h_regress_para.(parameters{pstep}).min,...
        'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')}, 'Units','normalized','Position',[0.41 0.05 0.23 0.2]);
    uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','text','String','Minimum: ',...
        'Units','normalized','Position',[0.30 0.05 0.11 0.2]);
    
    h_regress_para.(parameters{pstep}).maxval = uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','edit','String',h_regress_para.(parameters{pstep}).max,...
        'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')}, 'Units','normalized','Position',[0.76 0.05 0.23 0.2]);
    uicontrol(h_regress_para.(parameters{pstep}).panel,'Style','text','String','Maximum: ',...
        'Units','normalized','Position',[0.65 0.05 0.11 0.2]);
    
    undo.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
end

% Callbacks
    function button_regress_process(~,~)
        stop_regression = false;
        set(h_regress_button_process,'String','Stop regression',...
            'CallBack',@button_regress_stop); drawnow;
        
        for pstep = 1:numel(parameters)
            undo.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
            settings.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).initial,'String'));
            param_min.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).minval,'String'));
            param_max.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).maxval,'String'));
        end
        % 1) make all inputs same order of magnitude
        param_coeffs = get_coeff([settings.hd_a,settings.hd_b,settings.hd_c,settings.volmax,settings.abs_compress,settings.therm_expan]);
        param_exp = get_exp([settings.hd_a,settings.hd_b,settings.hd_c,settings.volmax,settings.abs_compress,settings.therm_expan]);
        clear settings % IMPORTANT SO AS NOT TO CROSS CONTAMINATE REGRESSION SUBFUNCTION
        % 2) set options
        options = optimset('Display','iter',...
            'OutputFcn',@button_regress_plot_iteration,...
            'MaxIter',str2double(get(h_regress_opt_maxiter,'String')),...
            'TolFun',str2double(get(h_regress_opt_tolfun,'String')));
        % 3) initiate iteration plot
        delete(get(h_regress_iter,'Children'))
        set(h_regress_iter,...
            'XLim',[0 str2double(get(h_regress_opt_maxiter,'String'))],...
            'XLimMode','manual');
        h_regress_iter_plot = plot(h_regress_iter,...
            [0:str2double(get(h_regress_opt_maxiter,'String'))],...
            nan(str2double(get(h_regress_opt_maxiter,'String'))+1,1),...
            'LineStyle','none','Marker','d');
        % 4) select appropriate cost function
        switch find(cellfun(@(x) x==1, get(h_regress_cost_radio,'Value')))
            case 1
                h_regress_scorefun = @button_regress_score_minmeanwh2o;
            case 2
                h_regress_scorefun = @button_regress_score_minmodewh2o;
            case 3
                h_regress_scorefun = @button_regress_score_minuddiff;
            case 4
                h_regress_scorefun = @button_regress_score_pitchbuoyspace;
            case 5
                %h_regress_scorefun = @
        end
        % 5) do regression AND reapply exponents
        if strcmpi(constrainRegression{get(h_regress_opt_constrain,'Value')},'on')
            for pstep = 1:numel(parameters)
                param_min.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).minval,'String'));
                param_max.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).maxval,'String'));
            end
            param_min = [param_min.hd_a,param_min.hd_b,param_min.hd_c,param_min.volmax,param_min.abs_compress,param_min.therm_expan] ./ (10.^param_exp);
            param_max = [param_max.hd_a,param_max.hd_b,param_max.hd_c,param_max.volmax,param_max.abs_compress,param_max.therm_expan] ./ (10.^param_exp);
            param_regressed = real(fmincon(@regress_parameters,param_coeffs,[],[],[],[],param_min,param_max,[],options) .* (10.^param_exp));
        else
            param_regressed = real(fminsearch(@regress_parameters,param_coeffs,options) .* (10.^param_exp));
        end
        % 6) joyfully announce it to the world by replacing settings and
        % variables in GUI.
        
        for pstep = 1:numel(parameters)
            if get(h_regress_para.(parameters{pstep}).regress,'Value') == 1
                data.gt_sg_settings.(parameters{pstep}) = param_regressed(pstep);
            end
            set(h_regress_para.(parameters{pstep}).initial,'String',data.gt_sg_settings.(parameters{pstep}));
        end
        
        button_regress_displaydefaults
        button_regress_plot_velocities(0,0)
        
        data.gt_sg_settings.regress_history(end+1,:) = {datestr(now),data.gt_sg_settings.hd_a,data.gt_sg_settings.hd_b,data.gt_sg_settings.hd_c,data.gt_sg_settings.volmax,data.gt_sg_settings.abs_compress,data.gt_sg_settings.therm_expan,' '};
        set(h_his_tables,'Data',data.gt_sg_settings.regress_history);
        
        set(h_regress_button_process,'String','Regress parameters',...
            'CallBack',@button_regress_process); drawnow;
        
        function score = regress_parameters(param)
            % Define settings
            params = param .* (10.^param_exp);
            settings.hd_a = params(1);
            settings.hd_b = params(2);
            settings.hd_c = params(3);
            settings.volmax = params(4);
            settings.abs_compress = params(5);
            settings.therm_expan = params(6);
            settings.length = data.gt_sg_settings.length;
            % Reset variables we don't want to regress
            for pstep = 1:numel(parameters)
                if get(h_regress_para.(parameters{pstep}).regress,'Value') == 0
                    settings.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
                end
            end
            % Run the model
            run_flightmodel(settings)
            
            % SCORE USING THE INDEX TO REMOVE BAD DATA
            score = h_regress_scorefun();
        end
        
        function stop = button_regress_plot_iteration(~,optimValues,~)
            stop = stop_regression;
            YData = get(h_regress_iter_plot,'YData');
            YData(get(h_regress_iter_plot,'XData') == optimValues.iteration) = optimValues.fval;
            set(h_regress_iter_plot,'YData',YData);
            drawnow;
        end
        
        function button_regress_stop(~,~)
            stop_regression = true;
        end
        
    end

    function button_regress_auto(~,~)
        for istep = 1:str2double(get(h_regress_opt_autopass,'String'))
            set(h_regress_cost_radio(1),'Value',1);
            set(h_regress_para.hd_a.regress,'Value',1);
            set(h_regress_para.hd_b.regress,'Value',1);
            set(h_regress_para.hd_c.regress,'Value',0);
            set(h_regress_para.volmax.regress,'Value',1);
            set(h_regress_para.abs_compress.regress,'Value',0);
            set(h_regress_para.therm_expan.regress,'Value',0);
            drawnow;
            button_regress_process; drawnow;
        end
    end

    function button_regress_store(~,~)
        for pstep = 1:numel(parameters)
            undo.(parameters{pstep}) = data.gt_sg_settings.(parameters{pstep});
            data.gt_sg_settings.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).initial,'String'));
        end
        button_regress_displaydefaults
    end

    function button_regress_undo(~,~)
        for pstep = 1:numel(parameters)
            set(h_regress_para.(parameters{pstep}).initial,'String',undo.(parameters{pstep}));
        end
        button_regress_store(0,0)
    end

    function button_regress_reset(~,~)
        for pstep = 1:numel(parameters)
            undo.(parameters{pstep}) = str2double(get(h_regress_para.(parameters{pstep}).initial,'String'));
            set(h_regress_para.(parameters{pstep}).initial,'String',data.gt_sg_settings.(parameters{pstep}));
            set(h_regress_para.(parameters{pstep}).maxval,'String',h_regress_para.(parameters{pstep}).max);
            set(h_regress_para.(parameters{pstep}).minval,'String',h_regress_para.(parameters{pstep}).min);
        end
    end

    function button_regress_regresscheckbox(~,~)
        for fstep = 1:numel(parameters)
            if get(h_regress_para.(parameters{fstep}).regress,'Value') == 1
                set(h_regress_para.(parameters{fstep}).initial,'Enable','on');
                button_regress_constrain(0,0);
            else
                set(h_regress_para.(parameters{fstep}).initial,'Enable','off');
                set(h_regress_para.(parameters{fstep}).maxval,'Enable','off');
                set(h_regress_para.(parameters{fstep}).minval,'Enable','off');
            end
        end
        
        button_regress_displaydefaults;
    end

    function button_regress_hullLength(~,~)
        data.gt_sg_settings.length = HullLength{get(h_regress_opt_length,'Value'),2};
    end

    function button_regress_constrain(~,~)
        for fstep = 1:numel(parameters)
            if get(h_regress_para.(parameters{fstep}).regress,'Value') == 1
                set(h_regress_para.(parameters{fstep}).maxval,'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')});
                set(h_regress_para.(parameters{fstep}).minval,'Enable',constrainRegression{get(h_regress_opt_constrain,'Value')});
            end
        end
    end

    function button_regress_displaydefaults
        uicontrol(h_tab(2),'Style','text','String',...
            {['HD_A: ',num2str(data.gt_sg_settings.hd_a)],...
            ['HD_B: ',num2str(data.gt_sg_settings.hd_b)],...
            ['HD_C: ',num2str(data.gt_sg_settings.hd_c)],...
            ['V_MAX: ',num2str(data.gt_sg_settings.volmax)],...
            ['A_CMP: ',num2str(data.gt_sg_settings.abs_compress)],...
            ['T_EXP: ',num2str(data.gt_sg_settings.therm_expan)],...
            ['Hull Length: ',num2str(data.gt_sg_settings.length)]},...
            'Units','normalized','Position',[0.87 0.505 0.12 0.16]);
    end

    function button_regress_plot_velocities(~,~)
        for gstep = 1:numel(parameters)
            settings.(parameters{gstep}) = str2double(get(h_regress_para.(parameters{gstep}).initial,'String'));
        end
        settings.length = data.gt_sg_settings.length;
        run_flightmodel(settings)
        
        glide_vert_spd = [data.flight(dives).glide_vert_spd];
        model_vert_spd = [data.flight(dives).model_vert_spd];
        cast_sign = sign([cast_dir{dives}]);
        depth = [data.hydrography(dives).depth];
        ind = [FRS.use_regress{dives}];
        
        mean_w_H2O = gt_sg_griddata(...
            cast_sign(ind),depth(ind),... % (x and y positions)
            glide_vert_spd(ind) - model_vert_spd(ind),... ( = w_H20 )
            [-1 1],[0:4:1000],@median); % (x and y bins)
        mean_glide = gt_sg_griddata(...
            cast_sign(ind),depth(ind),... % (x and y positions)
            glide_vert_spd(ind),...
            [-1 1],[0:4:1000],@median); % (x and y bins)
        mean_model = gt_sg_griddata(...
            cast_sign(ind),depth(ind),... % (x and y positions)
            model_vert_spd(ind),...
            [-1 1],[0:4:1000],@median); % (x and y bins)
        
        % Plot prelim state for future comparison
        % Switch previous to gray, and delete even older one
        persistent h_regress_vis_glide_old h_regress_vis_model_old h_regress_vis_wh2o_old h_regress_vis_zero_old...
            h_regress_vis_glide h_regress_vis_model h_regress_vis_wh2o h_regress_vis_zero
        try
            delete(h_regress_vis_glide_old);
            delete(h_regress_vis_model_old);
            delete(h_regress_vis_wh2o_old);
            delete(h_regress_vis_zero_old);
            h_regress_vis_glide_old = h_regress_vis_glide;
            h_regress_vis_model_old = h_regress_vis_model;
            h_regress_vis_wh2o_old = h_regress_vis_wh2o;
            h_regress_vis_zero_old = h_regress_vis_zero;
            set(h_regress_vis_glide_old,'Color',[0.8 0.8 0.8],'LineStyle','--');
            set(h_regress_vis_model_old,'Color',[0.8 0.8 0.8],'LineStyle','--');
            set(h_regress_vis_wh2o_old,'Color',[0.8 0.8 0.8],'LineStyle','--');
        end
        h_regress_vis_wh2o = plot(h_regress_vis,[mean_w_H2O(:,1);mean_w_H2O(end:-1:1,2)],[0:4:1000,1000:-4:0],'-g');
        h_regress_vis_glide = plot(h_regress_vis,[mean_glide(:,1);mean_glide(end:-1:1,2)],[0:4:1000,1000:-4:0],'-b');
        h_regress_vis_model = plot(h_regress_vis,[mean_model(:,1);mean_model(end:-1:1,2)],[0:4:1000,1000:-4:0],'-r');
        h_regress_vis_zero = plot(h_regress_vis,[0 0],get(h_regress_vis,'YLim'),'-k');
        axis(h_regress_vis,'ij','tight');
    end

    function score = button_regress_score_minmeanwh2o
        % Score function: root mean square difference of the glide and
        % model vertical speeds. This should effectively minimise w_H20 and
        % hopefully also correct slanted w_H20 linked to thermal expansion
        % in high thermal gradient regions.
        w_H2O = abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]);
        score = nanmean(w_H2O([FRS.use_regress{dives}]));
        % TODO: grid in time to avoid aliasaing of vertical sampling of
        % vertically moving features??
    end

    function score = button_regress_score_minmodewh2o
        % Score function: root mean square difference of the glide and
        % model vertical speeds. This should effectively minimise w_H20 and
        % hopefully also correct slanted w_H20 linked to thermal expansion
        % in high thermal gradient regions.
        w_H2O = abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]);
        score = mode(round(w_H2O([FRS.use_regress{dives}])*10000000))/10000000;
        % TODO: grid in time to avoid aliasaing of vertical sampling of
        % vertically moving features??
    end

    function score = button_regress_score_minuddiff
        % Score function: root mean square difference of the up and down
        % estimates of vertical velocity across all dives. This requires
        % gridding of all the estimates and then an RMSD.
        % 1. Grid all up/down estimates of w_H20
        glide_vert_spd = [data.flight(dives).glide_vert_spd];
        model_vert_spd = [data.flight(dives).model_vert_spd];
        cast_Dir = [cast_dir{dives}];
        depth = [data.hydrography(dives).depth];
        ind = [FRS.use_regress{dives}];
        w_H2O = gt_sg_griddata(...
            cast_Dir(ind),depth(ind),... % (x and y positions)
            glide_vert_spd(ind) - model_vert_spd(ind),... ( = w_H20 )
            [-max(dives):max(dives)],[0:4:1000],@median); % (x and y bins)
        % w_H2O = downcasts on the left, up casts on the right, with dives
        % mirrored, so "fold" the matrix in the middle and substract the
        % two sides.
        w_H2O = w_H2O(:,max(dives)+1:-1:1) - w_H2O(:,max(dives)+1:end);
        % TODO: Do we want to weight bins by number of samples in each bin
        % (could be useful for severely skewed profile depth distributions)
        score = nanmean(abs(w_H2O(:)));
    end

    function score = button_regress_score_pitchbuoyspace
        % Grid data in buoyancy and pitch space. Return nanmedian of gridded
        % surface, and try to minimise that. Minimising may not be the best
        % but I'm currently struggling to think of computationally cheap
        % way to test if the resulting surface is "flat"... So all around 0
        % will do.
        % Since boyancy is dependent on some regressed parameters, we'll
        % grid in dP/dt pitch space as those are constant.
        % This should provide some benefit as it will provide equal
        % weighting around the parameter space rather than skewing the
        % flight model towards steady sub thermocline conditions where we
        % tend to have much less variance in pitch and buoyancy.
        w_H2O = gt_sg_griddata(...
            [data.flight(dives).glide_vert_spd],... % X
            [data.eng(dives).pitchAng],... % Y
            abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]),... % V
            [-1:0.02:1],... % xi
            [-80:80],... % yi
            @mean,... % Median rather than mean.
            0,... % No interpolation
            0); % No plots
        w_Num = gt_sg_griddata(...
            [data.flight(dives).glide_vert_spd],... % X
            [data.eng(dives).pitchAng],... % Y
            abs([data.flight(dives).glide_vert_spd]-[data.flight(dives).model_vert_spd]),... % V
            [-1:0.02:1],... % xi
            [-80:80],... % yi
            @numel,... % Count the points
            0,... % No interpolation
            0); % No plots
        score = nanmean(w_H2O(w_Num > 20));
    end

%% Tab 3 - Visualisation
h_vis_refresh = uicontrol(h_tab(3),'Style','pushbutton','String','Refresh',...
    'Units','normalized','Position',[0.86 0.94 0.13 0.05],...
    'CallBack',@button_vis_refresh);

h_vis_plot.wh2o = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.08 0.57 0.44 .4],'Parent',h_tab(3));
h_vis_plot.wh2odiff = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.08 0.08 0.44 .4],'Parent',h_tab(3));
h_vis_plot.N2 = axes('YAxisLocation','right','Box','on',...
    'Units','normalized','Position',[0.65 0.08 0.34 .85],'Parent',h_tab(3));

vars.abs_salinity = gt_sg_griddata(...
    [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
    [data.hydrography(dives).abs_salinity],...
    [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)
vars.cons_temp = gt_sg_griddata(...
    [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
    [data.hydrography(dives).cons_temp],...
    [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)
vars.pressure = gt_sg_griddata(...
    [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
    [data.hydrography(dives).pressure],...
    [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)

X = [-max(dives):max(dives)];
for kstep=1:numel([-max(dives):max(dives)])
    try
        [vars.N2(:,kstep),~] = gsw_Nsquared(...
            vars.abs_salinity(:,kstep),...
            vars.cons_temp(:,kstep),...
            vars.pressure(:,kstep),...
            data.gps_postdive(abs(X(kstep)),1));
    end
end

    function button_vis_refresh(~,~)
        FRS.dives = gt_sg_sub_check_dive_validity(data);
        gt_sg_sub_echo({'Applying hydrodynamic calculations to all dives.'});
        run_flightmodel(data.gt_sg_settings);
        % Plot difference of up and down casts
        % 1. Grid all up/down estimates of w_H20
        w_H2O = gt_sg_griddata(...
            abs([cast_dir{dives}]),[data.hydrography(dives).depth],... % (x and y positions)
            [data.flight(dives).glide_vert_spd] - [data.flight(dives).model_vert_spd],... ( = w_H20 )
            [1:max(dives)],[10:4:990],@median); % (x and y bins)
        w_H2O_percast = gt_sg_griddata(...
            [cast_dir{dives}],[data.hydrography(dives).depth],... % (x and y positions)
            [data.flight(dives).glide_vert_spd] - [data.flight(dives).model_vert_spd],... ( = w_H20 )
            [-max(dives):max(dives)],[10:4:990],@median); % (x and y bins)
        % w_H2O = downcasts on the left, up casts on the right, with dives
        % mirrored, so "fold" the matrix in the middle and substract the
        % two sides.
        w_H2O_diff = w_H2O_percast(:,max(dives):-1:1) - w_H2O_percast(:,max(dives)+2:end);
        
        h_vis_plot.wh2o_plot = pcolor(h_vis_plot.wh2o,[1:max(dives)],[10:4:990],real(w_H2O));
        set(h_vis_plot.wh2o,'YDir','reverse')
        xlabel(h_vis_plot.wh2o,'Dive Number'); ylabel(h_vis_plot.wh2o,'Depth (m)');
        set(h_vis_plot.wh2o_plot,'EdgeColor','none')
        h_vis_plot.wh2o_cbar = colorbar('peer',h_vis_plot.wh2o); ylabel(h_vis_plot.wh2o_cbar,{'Estimated W_H_2_O'});
        caxis(h_vis_plot.wh2o,[-1 1]*0.02);
        
        h_vis_plot.wh2odiff_plot = pcolor(h_vis_plot.wh2odiff,[1:max(dives)],[10:4:990],real(w_H2O_diff));
        set(h_vis_plot.wh2odiff,'YDir','reverse')
        xlabel(h_vis_plot.wh2odiff,'Dive Number'); ylabel(h_vis_plot.wh2odiff,'Depth (m)');
        set(h_vis_plot.wh2odiff_plot,'EdgeColor','none')
        h_vis_plot.wh2odiff_cbar = colorbar('peer',h_vis_plot.wh2odiff); ylabel(h_vis_plot.wh2odiff_cbar,{'Absolute difference between up and downcast estimates of W_H_2_O','Ideally, it should appear as random noise dissimilar T&S structures or dive profile shape.'});
        caxis(h_vis_plot.wh2odiff,[-0.02 0.02]);
        
        w_H2O_N2 = w_H2O_percast(1:end-1,:) + diff(w_H2O_percast);
        h_vis_plot.N2_plot = plot(h_vis_plot.N2,...
            vars.N2(:),...
            w_H2O_N2(:).^2,...
            '-k','Marker','.','LineStyle','none');
        xlabel(h_vis_plot.N2,'N^2 (s^-^2)'); ylabel(h_vis_plot.N2,'W_H_2_O^2 (cm^2 s^-^2)');
        set(h_vis_plot.N2,'XLim',[0 5e-4],'YLim',[0 0.003]);
    end

% W vs Temp, Depth, Time
% Up vs Down
% Histograms like in Frajka Williams

%% Tab 4 - History
h_param_save = uicontrol(h_tab(4),'Style','pushbutton','String','Save notes',...
    'Units','normalized','Position',[0.86 0.94 0.13 0.05],...
    'CallBack',@button_his_save);

columnname = {'Date','hd_a','hd_b','hd_c','volmax','abs_compress','therm_expan','Notes'};
columnformat = {'char','char','char','char','char','char','char','char'};

if ~isfield(data.gt_sg_settings,'regress_history')
    data.gt_sg_settings.regress_history = {datestr(now),data.gt_sg_settings.hd_a,data.gt_sg_settings.hd_b,data.gt_sg_settings.hd_c,data.gt_sg_settings.volmax,data.gt_sg_settings.abs_compress,data.gt_sg_settings.therm_expan,' '};
end

h_his_tables = uitable(h_tab(4),'Data', data.gt_sg_settings.regress_history,...
    'ColumnName', columnname,...
    'ColumnFormat', columnformat,...
    'ColumnEditable', [false false false false false false false true],...
    'ColumnWidth',{200 150 150 150 150 150 150 200},...
    'RowName',[],...
    'Units','normalized','Position',[0.05 0.05 0.9 0.85],...
    'CellEditCallback',@button_his_save);

    function button_his_save(~,~)
        data.gt_sg_settings.regress_history = get(h_his_tables,'Data');
    end

%% Tab 5 - HEEEEEEEEELP ME !
h_auto_process = uicontrol(h_tab(5),'Style','pushbutton','String','Automatic regression',...
    'Units','normalized','Position',[0.86 0.94 0.13 0.05],...
    'CallBack',@button_regress_auto);

uicontrol('Parent',h_tab(5),'Style','text','String',HelpInfo,...
    'Units','normalized','Position',[0.05 0.05 0.5 0.9])

%% Subfunctions
    function out = indicesIn(list,sublist)
        [~, out, ~] = intersect(list,sublist);
    end

% Create get_exp handle to make all regressed coefficients of similar
% units (works better).
get_exp = @(x) floor(log10(x));
get_coeff = @(x) x.*10.^-get_exp(x);

    function gt_sg_flightmodelregression_calcCurrents
        % Calculate vertical velocities, depth-averaged currents and interpolated GPS positions
        if isfield(data.flight,'model_spd')
            gt_sg_sub_echo({'Estimating up/downwelling, depth-average currents and underwater coordinates.'});
            data = gt_sg_sub_currents(data);
        else
            gt_sg_sub_echo({'No flight model output available.',...
                'Cannot estimate up/downwelling, depth-averaged currents or',...
                'coordinates of the glider underwater.',...
                'WARNING: Flight model data should be present, something has gone wrong'});
        end
    end

%% Flight model scripts
%%%%%%%%%%%%%% START OF FLIGHT MODEL ROUTINES %%%%%%%%%%%%%%
    function run_flightmodel(settings)
        for istep = FRS.dives
            % settings. should have volmax,hd_a,hd_b,hd_c,abs_compress,therm_expan,length
            % Determine glider buoyancy based on volmax and in situ density
            % Calculate volume
            vbd = data.eng(istep).vbdCC; % cm^3
            vol0 = settings.volmax + (data.log(istep).C_VBD - data.gt_sg_settings.vbd_min_cnts)/data.gt_sg_settings.vbd_cnts_per_cc; % cm^3
            vol = (vol0 + vbd) .* ...
                exp(-settings.abs_compress * data.hydrography(istep).pressure + settings.therm_expan * (data.hydrography(istep).temp - data.gt_sg_settings.temp_ref)); % cm^3
            
            % Calculate buoyancy of the glider
            % Buoyancy units
            %   kg + (kg.m-3 * cm^3 * 10^-6)
            % = kg + (kg.m-3 * m^3)
            % = kg
            data.flight(istep).buoyancy =  -data.gt_sg_settings.mass + ( data.hydrography(istep).rho .* vol * 1.e-6 ); % kg
            
            % Calculate glide slope and speed
            if ~hd_array
                [data.flight(istep).model_spd, data.flight(istep).model_slope]... % m.s-1, degrees
                    = flightvec(...
                    data.flight(istep).buoyancy,... % kg
                    data.eng(istep).pitchAng,... % degrees
                    settings.length,... % Hull length in meters, excluding antenna
                    settings.hd_a,... % rad^-1
                    settings.hd_b,... % m^1/4 . kg^1/4 . s^-1/2
                    settings.hd_c,... % rad^-2
                    data.gt_sg_settings.rho0,...
                    istep); % kg.m-3
            else
                [data.flight(istep).model_spd, data.flight(istep).model_slope]... % m.s-1, degrees
                    = flightvec(...
                    data.flight(istep).buoyancy,... % kg
                    data.eng(istep).pitchAng,... % degrees
                    settings.length,... % Hull length in meters, excluding antenna
                    settings.hd_a(istep),... % rad^-1
                    settings.hd_b(istep),... % m^1/4 . kg^1/4 . s^-1/2
                    settings.hd_c(istep),... % rad^-2
                    data.gt_sg_settings.rho0,...
                    istep); % kg.m-3
            end
            
            % Determine vertical and horizontal speed
            data.flight(istep).model_horz_spd = data.flight(istep).model_spd.*cos(data.flight(istep).model_slope*pi/180);
            data.flight(istep).model_vert_spd = data.flight(istep).model_spd.*sin(data.flight(istep).model_slope*pi/180);
        end
    end
%% ADDING SEPARATE FUNCTION FOR 1:NUMEL(DIVE) LENGTH HD_* PARAMETERS TO SAVE TIME ON REGRESSION BY AVOIDING SUPERFLUOUS "IF" STATEMENT

    function [ umag, thdeg ] = flightvec( bu, ph, xl, a, b, c, rho0, istep)
        % ********************************************************************************
        %	function flightvec0(bu, ph, xl, a, b, c, rho0 )
        %		Solves unaccelerated flight equations iteratively
        %		for speed magnitude umag and glideangle thdeg
        % ********************************************************************************
        
        gravity = gsw_grav(data.gps_postdive(istep,1),...
            data.hydrography(istep).pressure); % m.s-2
        
        th = (pi/4)*sign(bu);	% initial slope set to 1 or -1
        
        buoyforce = gravity .* bu; % kg.m.s-2
        
        q = ( sign(bu).*buoyforce./(xl*xl*b) ).^(4/3); 	% dynamic pressure for vertical flight
        alpha = 0.;
        
        tol = 0.001;
        j = 0;
        q_old = zeros(size(bu));
        param = ones(size(bu));
        valid = find( bu ~= 0 & sign(bu).*sign(ph) > 0 );
        umag = zeros(size(bu));
        thdeg = zeros(size(bu));
        
        while(~isempty( find( abs( (q(valid)-q_old(valid))./q(valid) ) > tol )) & j <= 15);
            
            q_old = q;
            param_inv = a*a*tan(th).*tan(th).*q.^(0.25)/(4*b*c);
            
            valid = find( param_inv > 1 & sign(bu).*sign(ph) > 0);	% valid solutions for param < 1
            
            param(valid) = 4*b*c./(a*a*tan(th(valid)).*tan(th(valid)).*q(valid).^(0.25));
            q(valid) = ( buoyforce(valid).*sin(th(valid))./(2*xl*xl*b*q(valid).^(-0.25)) ).*  ...
                (1 + sqrt(1-param(valid)));
            alpha(valid) = ( -a*tan(th(valid))./(2*c) ).*(1 - sqrt(1-param(valid)));
            thdeg(valid) = ph(valid) - alpha(valid);
            
            stall = find( param_inv <= 1 | sign(bu).*sign(ph) < 0 );	% stall if param >= 1
            
            % TODO: REMOVE ME
            stall = [];
            %
            
            q(stall) = 0.;
            thdeg(stall) = 0.;
            
            th = thdeg*pi/180;
            
            j = j+1;
            % TODO: Should this be replaced with actual density.....??
            umag = sqrt( 2*q/rho0 );
        end
    end

    function [ umag, thdeg ] = flightvec2( bu, ph, xl, a, b, c, rho0, istep)
    %function [umag,thdeg] = flightvec2(bu,ph,vmtime,xl,min_rel_q_inc,max_num_iters,flight_consts)
    %function [umag,thdeg] = flightvec2(bu,ph,vmtime,xl,min_rel_q_inc,max_num_iters,flight_consts,rho0,interp_stalls)
        % Copyright (c) 2006-2012, 2014 by University of Washington.  All rights reserved.
        %
        % This file contains proprietary information and remains the
        % unpublished property of the University of Washington. Use, disclosure,
        % or reproduction is prohibited.
        
        % [umag,thdeg] = flightvec2(bu,ph,vmtime,xl,min_rel_q_inc,max_num_iters,flight_consts,rho0)
        %
        % solves unaccelerated flight equations iteratively
        % for along glideslope speed umag and glideangle thdeg
        % umag and thdeg describe flight in the presence of zero current
        %
        % inputs are: the net buoyancy (bu), pitch in degrees (ph),
        %             time since start of dive (vmtime), sg fairing length (xl),
        %             min_rel_q_inc: the desired tolerance for relative q change,
        %                            between successive iterations,
        %             max_num_iters: the maximum number of iterations if
        %                            min_rel_q_inc is not reached,
        %             flight_consts = [hd_a hd_b hd_c], and rho0 a ref. density
        
%         interp_stalls = true;
%         vmtime = ??;
%         min_rel_q_inc = ??;
%         max_num_iters = ??;
%         flight_consts = ??;
        
        
        gravity = gsw_grav(data.gps_postdive(istep,1),...
            data.hydrography(istep).pressure); % m.s-2
        %gravity = 9.82;
        % TODO: export gravity to a previous calculation to avoid
        % recalculating everytime. Makes regressing the flight model very
        % slow....
        
        g2kg = 0.001;
        m2cm = 100;
        
        % TODO: what is hd_s???
        %hd_a is in 1/deg. units, hd_b has dimensions q^(1/4), hd_c is in 1/deg.^2 units
        hd_a = flight_consts(1); hd_b = flight_consts(2); hd_c = flight_consts(3); hd_s = flight_consts(4);
        
        % trace_comment(sprintf('hd_a = %f', hd_a));
        % trace_comment(sprintf('hd_b = %f', hd_b));
        % trace_comment(sprintf('hd_c = %f', hd_c));
        % trace_comment(sprintf('hd_s = %f', hd_s));
        % trace_comment(sprintf('glider_length = %f', xl));
        % trace_comment(sprintf('rho0 = %f', rho0));
        % trace_array('buoyancy', bu)
        % trace_array('pitch', ph)
        
        %convert net buoyancy to force dimensions (N units)
        buoyforce = g2kg*gravity*bu;
        
        % initialize flight variables
        % initial glideslope set to 1 or -1 depending on whether sg is pos. or neg. buoyant
        % if sign(buoyforce)=0, that value will not be a member of valid below
        % We deliberately start far away from the soln so both q and th, set independently here,
        % are able to relax together to a consistent soln.
        th = (pi/4)*sign(buoyforce);
        % initial dynamic pressure q for vertical flight (always a positive quantity)
        % q is determined from the drag eqn. (Eqn. 2 in Eriksen et al.) by assuming alpha = 0
        % and that we are completely vertical (sin(90) = 1) so all drag, no lift
        
        % BUG: Depending on initial abc values this formulation of 'q_old' will be arbitrarily close to q below
        % and terminate the loop after a single iteration with poor th (and q).
        % If the loop is forced to run at least twice, both q and th are updated and we make proper progress toward the soln.
        %DEAD q = (abs(buoyforce.*sin(th))/(xl*xl*hd_b)).^(1/(1+hd_s));
        % This original version, without the sin(th) term, ensures q and q_old are sufficiently different
        % that we don't stall out the loop; we also ensure we perform two loops to get q close first, then th
        q = (sign(bu).*buoyforce/(xl*xl*hd_b)).^(1/(1+hd_s)); 	% dynamic pressure for vertical flight
        % trace_array('q_init', q);
        
        all_i = [1:prod(size(q))];
        %initialize variables used in while loop
        param = NaN*ones(size(bu));
        alpha = NaN*ones(size(bu));
        thdeg = NaN*ones(size(bu));
        
        %this loop iterates the dynamic pressure (q) to get glidespeed
        %and the attack angle (alpha) to get glideangle
        %buoyancy is taken to be an accurately known quantity and is not iterated
        j = 1;
        test = (1+0.1)*min_rel_q_inc*ones(size(q)); %force loop to run at least once
        while((~isempty(find(abs(test) > min_rel_q_inc)) & j <= max_num_iters) | j < 3) % ensure at least 2 iterations
            % param_inv is the reciprocal of the term subtracted
            % from 1 under the radical in the solution equations (Eqs. 7-8)
            % CCE recalls he did it this way because early on th would go to zero
            % and so the param became infinite.  Using the inverse avoids this problem.
            % here param_inv is estimated using the current th
            param_inv = hd_a^2*tan(th).^2.*q.^(-hd_s)/(4*hd_b*hd_c);
            
            %valid solutions exist for param_inv > 1 (zero or complex otherwise)
            %also only consider points where buoyancy and pitch are both up or both down
            valid = find(param_inv > 1 & sign(bu).*sign(ph) > 0);
            
            %th and q are not zero for valid points, since otherwise param_inv would = 0
            param(valid) = 1./param_inv(valid);
            %hold current q in q_old
            q_old = q;
            %Eq. 7 in the reference, obtained using the quadratic formula ...
            %q^(1/4) considered to vary slowly compared to q
            %NOTE the q eqn use 1.0 + sqrt and the alpha eqn must use 1.0 - sqrt
            q(valid) = buoyforce(valid).*sin(th(valid)).*q(valid).^(-hd_s)./(2*xl^2*hd_b) ...
                .*(1 + sqrt(1-param(valid)));
            
            % NOTE that once a point stalls, it can never recover
            % even though our guess at th and q might be off...
            % the problem is we don't know how to relax either
            % locally to possibly ensure or encourage a lasting
            % non-stall solution.
            stall = setdiff(all_i,valid);
            q(stall) = NaN;
            
            %calculate relative difference to see if next iteration is necessary
            test = (q-q_old)./q;
            %Eq. 8 in the reference with fixed typos
            % hd_a is 1/deg, hd_c is 1/deg^2 so overall alpha is in degrees
            alpha(valid) = -hd_a*tan(th(valid))./(2*hd_c).*(1 - sqrt(1-param(valid)));
            alpha(stall) = NaN;
            %glideangle is the difference between pitch and angle of attack
            thdeg(valid) = ph(valid) - alpha(valid);
            thdeg(stall) = NaN;
            
            %switch theta to radians
            th = thdeg*pi/180;
            
            %    trace_array(sprintf('q_%d',j),q);
            %    trace_array(sprintf('th_%d',j),th);
            %    trace_array(sprintf('param_inv_%d',j),param_inv);
            
            %increase iteration number
            j = j+1;
        end
        
        
        if interp_stalls % optional
            % An attempt to interpolate across apogee but if there were other reasons for stalls
            % we'd interpolate there too -- and get bad values
            [m,n] = size(vmtime);
            if m == 1 | n == 1 % vector?
                
                %for values of th and q near apogee for which model couldn't be used,
                %linearly interpolate to determine these values
                good_inds = find(isfinite(vmtime) & isfinite(thdeg));
                if length(good_inds) >= 2
                    thdeg = interp1(vmtime(good_inds),thdeg(good_inds),vmtime,'linear');
                    %force leading and trailing values where NaNs occur since interpolation wasn't possible
                    %to be 0 ... otherwise extreme values may be produced here
                    good_thdeg_inds = find(isfinite(thdeg));
                    if good_thdeg_inds(1) ~= 1
                        thdeg(1:good_thdeg_inds(1)-1) = 0;
                    end
                    if good_thdeg_inds(end) ~= length(thdeg)
                        thdeg(good_thdeg_inds(end)+1:end) = 0;
                    end
                end
                
                good_inds_2 = find(isfinite(vmtime) & isfinite(q));
                if length(good_inds_2) >= 2
                    q = interp1(vmtime(good_inds_2),q(good_inds_2),vmtime,'linear');
                    good_q_inds = find(isfinite(q));
                    if good_q_inds(1) ~= 1
                        q(1:good_q_inds(1)-1) = 0;
                    end
                    if good_q_inds(end) ~= length(q)
                        q(good_q_inds(end)+1:end) = 0;
                    end
                end
            end
        end
        %glidespeed in cm/s
        umag = m2cm*sqrt(2*q/rho0);
    end
%%%%%%%%%%%%%% END OF FLIGHT MODEL ROUTINES %%%%%%%%%%%%%%

%% WAIT FOR GUI CLOSE IF RUNNING INTERACTIVE, OTHERWISE RUN AUTO FUNCTION AND QUIT

if all([...
        numel(data.gt_sg_settings.hd_a) == numel(data.hydrography),...
        numel(data.gt_sg_settings.hd_b) == numel(data.hydrography),...
        numel(data.gt_sg_settings.hd_c) == numel(data.hydrography)])
    hd_array = 1;
    run_flightmodel(data.gt_sg_settings);
    gt_sg_sub_echo({'Applying variable hydrodynamic parameters define in gt_sg_settings.','Bypassing regression GUI and proceeding straight to calculations.'});
    gt_sg_flightmodelregression_calcCurrents;
    close(h_gui);
    return;
else
    hd_array = 0;
end

if auto
    button_regress_auto(0,0);
    FRS.dives = gt_sg_sub_check_dive_validity(data);
    run_flightmodel(data.gt_sg_settings);
    close(h_gui);
    gt_sg_sub_echo(['Exporting new flight coefficients to gt_sg_settings.']);
    gt_sg_sub_echo({['To save time in future, we suggest adding the following to sg_calib_constants.m'],...
        ['  hd_a = ',num2str(data.gt_sg_settings.hd_a,9),';'],...
        ['  hd_b = ',num2str(data.gt_sg_settings.hd_b,9),';'],...
        ['  hd_c = ',num2str(data.gt_sg_settings.hd_c,9),';'],...
        ['  volmax = ',num2str(data.gt_sg_settings.volmax,9),';'],...
        ['  abs_compress = ',num2str(data.gt_sg_settings.abs_compress,9),';'],...
        ['  therm_expan = ',num2str(data.gt_sg_settings.therm_expan,9),';']...
        });
    gt_sg_flightmodelregression_calcCurrents;
    return;
end

run_flightmodel(data.gt_sg_settings);
uiwait(h_gui)
FRS.dives = gt_sg_sub_check_dive_validity(data);
gt_sg_sub_echo({'Applying hydrodynamic calculations to all dives.'});
run_flightmodel(data.gt_sg_settings);
data.flight = data.flight(:);

gt_sg_sub_echo(['Exporting new flight coefficients to gt_sg_settings.']);
gt_sg_sub_echo({['To save time in future, we suggest adding the following to sg_calib_constants.m'],...
    ['  hd_a = ',num2str(data.gt_sg_settings.hd_a,9),';'],...
    ['  hd_b = ',num2str(data.gt_sg_settings.hd_b,9),';'],...
    ['  hd_c = ',num2str(data.gt_sg_settings.hd_c,9),';'],...
    ['  volmax = ',num2str(data.gt_sg_settings.volmax,9),';'],...
    ['  abs_compress = ',num2str(data.gt_sg_settings.abs_compress,9),';'],...
    ['  therm_expan = ',num2str(data.gt_sg_settings.therm_expan,9),';']...
    });
gt_sg_flightmodelregression_calcCurrents;

end
