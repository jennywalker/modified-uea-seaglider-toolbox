function griddedOutput = gt_sg_export_netcdf(data,filenameprefix,dives,binsize,flag,interp)
% Exports data structure to netcdf format.
% filenameprefix = Prefix to append before dive number in filename.
%
% data = Data structure output by the gt_sg routines. Must contain the fields eng and sg_calib_const.
% dives = Dive numbers to export. 0 exports all available dives.
% binsize = Vertical bin size in metres
% flag = How to handle flagged data:
%       -1 : Do not use flags, leave all flagged data as is
%        0 : Apply flags on a variable to variable basis (eg. salinity 
%            flags only affect salinity)
%        1 : Propagate all flags across all variables (eg. flagged salinity
%            will make chorophyll)
% interp: default is 0 for none. Dimension along which to perform a 1-D
%       interpolation of the data. 1 is for vertical, 2 for horizontal.
%

%% Set dives to process
if nargin < 6
    interp = 0;
end
if nargin < 5
    flag = -1; % -1 apply none, 0 apply var only, 1 propagate all
end
if nargin < 4
    binsize = 3;
end
if nargin < 3
    dives = gt_sg_sub_check_dive_validity(data);
end
if dives == 0
    dives = gt_sg_sub_check_dive_validity(data);
end
if nargin < 2
    filenameprefix = ['SG',num2str(data.log(dives(1)).ID),'_',data.sg_calib_const.mission_title];
end
%% Prepare flags
if flag == -1
    flags = ones(size([data.hydrography(dives).time]));
elseif flag == 1
    flags = cellfun(@(x) [data.flag(dives).(x)],fieldnames(data.flag),'Uni',0);
    string = arrayfun(@(x) [ ' | flags{',num2str(x),'}'      ],[1:numel(flags)],'Uni',0);
    eval(['flags = cast(~(0 ',string{:},'),''double'');']);
    flags(flags == 0) = NaN;
end

%% Assemble ancilliary data
for dstep = dives
    data.hydrography(dstep).lat = data.flight(dstep).trajectory_latlon_estimated(:,1)';
    data.hydrography(dstep).lon = data.flight(dstep).trajectory_latlon_estimated(:,2)';
    data.hydrography(dstep).dive = ones(size(data.hydrography(dstep).time)) .* dstep;
    data.hydrography(dstep).profile = data.hydrography(dstep).dive;
    data.hydrography(dstep).profile(data.hydrography(dstep).max_pressure_index:end) = data.hydrography(dstep).profile(data.hydrography(dstep).max_pressure_index:end) + 0.5;
end

DICT = gt_sg_sub_load_netcdf_dict;

%% Create timeseries file
ncid = netcdf.create([filenameprefix,'_timeseries.nc'],'CLOBBER');

netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'About','Made with the UEA gt_sg_ Seaglider toolbox');
netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'Created',datestr(now));
netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'Info','Development version, please contact BY Queste, b.queste@uea.ac.uk, for info');

griddedOutput.time = [data.hydrography.time];
fields = fieldnames(griddedOutput);
for fstep = 1:numel(fields)
    dimid.(fields{fstep}) = netcdf.defDim(ncid, fields{fstep}, numel(griddedOutput.(fields{fstep})));
    ID.(fields{fstep}) = netcdf.defVar(ncid,fields{fstep},'double',dimid.(fields{fstep}));
end

fields = setxor('time',...
    fieldnames(data.hydrography));

for fstep = 1:numel(fields)
    if size([data.hydrography.(fields{fstep})]) == size([data.hydrography.time])
        ID.(fields{fstep}) = netcdf.defVar(ncid,fields{fstep},'double',[dimid.time]);
        
        if isfield(DICT.hydrography,fields{fstep})
            atts = fieldnames(DICT.hydrography.(fields{fstep}));
            for astep = 1:numel(atts)
                netcdf.putAtt(ncid,ID.(fields{fstep}),atts{astep}, DICT.hydrography.(fields{fstep}).(atts{astep}) );
            end
        end
    end
end
netcdf.endDef(ncid);

fields = fieldnames(data.hydrography);
for fstep = 1:numel(fields)
    if size([data.hydrography.(fields{fstep})]) == size([data.hydrography.time])
        
        if flag == 0 
            if isfield(data.flag,fields{fstep})
                flags = cast(~[data.flag(dives).(fields{fstep})],'double');
                flags(flags ~= 1) = nan;
            else
                flags = ones(size([data.hydrography(dives).(fields{fstep})]));
            end
        end
        
        netcdf.putVar(ncid,ID.(fields{fstep}),[data.hydrography.(fields{fstep})].*flags);
    end
end
netcdf.close(ncid)
clear ncid

%% Create gridded profiles
ncid = netcdf.create([filenameprefix,'_binned.nc'],'NC_WRITE');

netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'About','Made with the UEA gt_sg_ Seaglider toolbox');
netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'Created',datestr(now));
netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),'Info','Development version, please contact BY Queste, b.queste@uea.ac.uk, for info');


bin.profile = [0:0.5:numel(data.hydrography)+1];
bin.depth = [0:binsize:1020];

X = bin.profile(1:end);
Y = bin.depth(1:end);

fields = fieldnames(bin);
for fstep = 1:numel(fields)
    dimid.(fields{fstep}) = netcdf.defDim(ncid, fields{fstep}, numel(bin.(fields{fstep})));
    ID.(fields{fstep}) = netcdf.defVar(ncid,fields{fstep},'double',dimid.(fields{fstep}));
end

fields = setxor({'depth','profile'},...
    fieldnames(data.hydrography));

for fstep = 1:numel(fields)
    if size([data.hydrography(dives).(fields{fstep})]) == size([data.hydrography(dives).time])
        disp(['Gridding ',fields{fstep}]);
        
        if flag == 0
            if isfield(data.flag,fields{fstep})
                flags = cast(~[data.flag(dives).(fields{fstep})],'double');
                flags(flags ~= 1) = nan;
            else
                flags = ones(size([data.hydrography(dives).(fields{fstep})]));
            end
        end
        
        griddedOutput.(fields{fstep}) = gt_sg_griddata(...
            [data.hydrography(dives).profile],[data.hydrography(dives).depth],... % (y and x positions)
            [data.hydrography(dives).(fields{fstep})].*flags,... % variable
            bin.profile,bin.depth,...
            @median,interp); % (x and y bins)
        
        ID.(fields{fstep}) = netcdf.defVar(ncid,fields{fstep},'double',[dimid.profile dimid.depth]);
        
        if isfield(DICT.hydrography,fields{fstep})
            atts = fieldnames(DICT.hydrography.(fields{fstep}));
            for astep = 1:numel(atts)
                netcdf.putAtt(ncid,ID.(fields{fstep}),atts{astep}, DICT.hydrography.(fields{fstep}).(atts{astep}) );
            end
        end
    end
end

netcdf.endDef(ncid);

netcdf.putVar(ncid,ID.profile,X);
netcdf.putVar(ncid,ID.depth,-Y);

fields = fieldnames(griddedOutput);
for fstep = 1:numel(fields)
    netcdf.putVar(ncid,ID.(fields{fstep}),griddedOutput.(fields{fstep})');
end

netcdf.close(ncid)

clear ncid
end

function DICT = gt_sg_sub_load_netcdf_dict
DICT.hydrography.profile.long_name = 'Profile number';
DICT.hydrography.profile.description = '';
DICT.hydrography.profile.unit = '';

DICT.hydrography.time.long_name = 'Time';
DICT.hydrography.time.description = 'Matlab DATENUM format, days since 0/0/0 00:00:00';
DICT.hydrography.time.unit = 'Days';

DICT.hydrography.dive.long_name = 'Dive number';
DICT.hydrography.dive.description = '';
DICT.hydrography.dive.unit = '';

DICT.hydrography.lat.long_name = 'Latitude';
DICT.hydrography.lat.description = '';
DICT.hydrography.lat.unit = '';

DICT.hydrography.lon.long_name = 'Longitude';
DICT.hydrography.lon.description = '';
DICT.hydrography.lon.unit = '';

DICT.hydrography.depth.long_name = 'Depth';
DICT.hydrography.depth.description = '';
DICT.hydrography.depth.unit = 'm';

DICT.hydrography.pressure.long_name = 'Pressure';
DICT.hydrography.pressure.description = '';
DICT.hydrography.pressure.unit = 'dbar';

DICT.hydrography.temp.long_name = 'In situ temperature';
DICT.hydrography.temp.description = '';
DICT.hydrography.temp.unit = 'deg C';

DICT.hydrography.cons_temp.long_name = 'Conservative temperature';
DICT.hydrography.cons_temp.description = 'TEOS-10';
DICT.hydrography.cons_temp.unit = 'deg C';

DICT.hydrography.salinity.long_name = 'Practical salinity';
DICT.hydrography.salinity.description = '';
DICT.hydrography.salinity.unit = 'PSU';

DICT.hydrography.abs_salinity.long_name = 'Absolute salinity';
DICT.hydrography.abs_salinity.description = 'TEOS-10';
DICT.hydrography.abs_salinity.unit = 'g kg-1';

DICT.hydrography.conductivity.long_name = 'Conductivity';
DICT.hydrography.conductivity.description = '';
DICT.hydrography.conductivity.unit = '';

DICT.hydrography.sigma0.long_name = 'Potential density';
DICT.hydrography.sigma0.description = '';
DICT.hydrography.sigma0.unit = 'kg m-3';

DICT.hydrography.rho.long_name = 'In situ density';
DICT.hydrography.rho.description = '';
DICT.hydrography.rho.unit = 'kg m-3';

DICT.hydrography.oxygen.long_name = 'Dissolved oxygen concentration';
DICT.hydrography.oxygen.description = '';
DICT.hydrography.oxygen.unit = 'umol kg-1';

DICT.hydrography.Chlorophyll.long_name = 'Chlorophyll a';
DICT.hydrography.Chlorophyll.description = '';
DICT.hydrography.Chlorophyll.unit = 'mg m-3';

DICT.hydrography.CDOM.long_name = 'Coloured dissolved organic matter';
DICT.hydrography.CDOM.description = '';
DICT.hydrography.CDOM.unit = '';

DICT.hydrography.Scatter_700.long_name = 'Optical backscattering at 700nm';
DICT.hydrography.Scatter_700.description = '';
DICT.hydrography.Scatter_700.unit = '';

DICT.hydrography.Scatter_650.long_name = 'Optical backscattering at 650nm';
DICT.hydrography.Scatter_650.description = '';
DICT.hydrography.Scatter_650.unit = '';

DICT.hydrography.Scatter_432.long_name = 'Optical backscattering at 432nm';
DICT.hydrography.Scatter_432.description = '';
DICT.hydrography.Scatter_432.unit = '';

DICT.hydrography.PAR.long_name = 'Photosynthetically active radiation';
DICT.hydrography.PAR.description = '';
DICT.hydrography.PAR.unit = '';

DICT.hydrography.w_H2O.long_name = 'Upwelling';
DICT.hydrography.w_H2O.description = '';
DICT.hydrography.w_H2O.unit = 'm s-1';

DICT.log = {...
    'CAPUPLOAD', 2, {};...
    'PHONE_DEVICE', 2, {};...
    'VBD_DBAND', 2, {};...
    'T_NO_W', 2, {};...
    'SURFACE_URGENCY', 2, {};...
    'VBD_PUMP_AD_RATE_APOGEE', 2, {};...
    'T_MISSION', 2, {};...
    'FIX_MISSING_TIMEOUT', 2, {};...
    'RAFOS_CORR_THRESH', 2, {};...
    'D_SAFE', 2, {};...
    'VBD_TIMEOUT', 2, {};...
    'P_OVSHOOT', 2, {};...
    'TGT_DEFAULT_LAT', 2, {};...
    'PITCH_TIMEOUT', 2, {};...
    '_XMS_NAKs', 2, {};...
    'D_NO_BLEED', 2, {};...
    'VBD_MAXERRORS', 2, {};...
    'SURFACE_URGENCY_TRY', 2, {};...
    'C_ROLL_DIVE', 2, {};...
    'SMARTDEVICE1', 2, {};...
    'LOGGERDEVICE1', 2, {};...
    'LOGGERDEVICE2', 2, {};...
    'LOGGERDEVICE3', 2, {};...
    'LOGGERDEVICE4', 2, {};...
    'TCM_TEMP', 2, {};...
    'VBD_MAX', 2, {};...
    'T_TURN', 2, {};...
    'RAFOS_DEVICE', 2, {};...
    'C_ROLL_CLIMB', 2, {};...
    'HEAD_ERRBAND', 2, {};...
    'ALTIM_TOP_MIN_OBSTACLE', 2, {};...
    'PRESSURE_YINT', 2, {};...
    'CALL_WAIT', 2, {};...
    'ROLL_TIMEOUT', 2, {};...
    'CALL_TRIES', 2, {};...
    'ALTIM_BOTTOM_PING_RANGE', 2, {};...
    'SEABIRD_C_J', 2, {};...
    'R_PORT_OVSHOOT', 2, {};...
    'SEABIRD_C_H', 2, {};...
    'C_VBD', 2, {};...
    'PITCH_CNV', 2, {};...
    'SEABIRD_C_G', 2, {};...
    'VBD_MIN', 2, {};...
    'RAFOS_PEAK_OFFSET', 2, {};...
    'PITCH_MIN', 2, {};...
    'HD_B', 2, {};...
    'HD_C', 2, {};...
    'PITCH_AD_RATE', 2, {};...
    'INT_PRESSURE_SLOPE', 2, {};...
    'ALTIM_SENSITIVITY', 2, {};...
    'HEADING', 2, {};...
    '_SM_DEPTHo', 2, {};...
    'COMM_SEQ', 2, {};...
    'KALMAN_USE', 2, {};...
    'TGT_NAME', 2, {};...
    'ALTIM_PING_DEPTH', 2, {};...
    'UPLOAD_DIVES_MAX', 2, {};...
    'RHO', 2, {};...
    'ALTIM_PING_DELTA', 2, {};...
    'ROLL_MAX', 2, {};...
    'D_CALL', 2, {};...
    'HD_A', 2, {};...
    'SMARTS', 2, {};...
    'D_FLARE', 2, {};...
    'T_GPS', 2, {};...
    'D_SURF', 2, {};...
    'PITCH_VBD_SHIFT', 2, {};...
    'PITCH_MAX', 2, {};...
    'APOGEE_PITCH', 2, {};...
    'C_PITCH', 2, {};...
    'MASS', 2, {};...
    'T_ABORT', 2, {};...
    'SPEED_FACTOR', 2, {};...
    'VBD_CNV', 2, {};...
    'COMPASS_DEVICE', 2, {};...
    'PRESSURE_SLOPE', 2, {};...
    'ALTIM_TOP_PING_RANGE', 2, {};...
    'T_GPS_CHARGE', 2, {};...
    'ID', 2, {};...
    'DEVICE4', 2, {};...
    'PITCH_GAIN', 2, {};...
    'DEVICE2', 2, {};...
    'DEVICE3', 2, {};...
    'DEVICE1', 2, {};...
    '_SM_ANGLEo', 2, {};...
    'T_TURN_SAMPINT', 2, {};...
    '_XMS_TOUTs', 2, {};...
    'CAPMAXSIZE', 2, {};...
    'CAPUPLOAD', 2, {};...
    'CAP_FILE_SIZE', 2, {};...
    'N_FILEKB', 2, {};...
    'ESCAPE_HEADING', 2, {};...
    'ROLL_CNV', 2, {};...
    'USE_ICE', 2, {};...
    'TGT_RADIUS', 2, {};...
    'R_STBD_OVSHOOT', 2, {};...
    'ICE_FREEZE_MARGIN', 2, {};...
    'TGT_AUTO_DEFAULT', 2, {};...
    'D_ABORT', 2, {};...
    'INT_PRESSURE_YINT', 2, {};...
    'COURSE_BIAS', 2, {};...
    'TCM_ROLL_OFFSET', 2, {};...
    'N_GPS', 2, {};...
    'HUMID', 2, {};...
    'T_DIVE', 2, {};...
    'MAX_BUOY', 2, {};...
    'FILEMGR', 2, {};...
    'D_TGT', 2, {};...
    'AH0_10V', 2, {};...
    'SEABIRD_T_H', 2, {};...
    'SEABIRD_T_I', 2, {};...
    'SEABIRD_T_J', 2, {};...
    'T_GPS_ALMANAC', 2, {};...
    'N_NOSURFACE', 2, {};...
    'RAFOS_CLK', 2, {};...
    'CF8_MAXERRORS', 2, {};...
    'ROLL_MIN', 2, {};...
    'SEABIRD_T_G', 2, {};...
    'DEEPGLIDER', 2, {};...
    'TGT_DEFAULT_LON', 2, {};...
    'DIVE', 2, {};...
    'RELAUNCH', 2, {};...
    'SIM_PITCH', 2, {};...
    'HEAPDBG', 2, {};...
    'SMARTDEVICE2', 2, {};...
    'USE_BATHY', 2, {};...
    'DEVICE6', 2, {};...
    'ESCAPE_HEADING_DELTA', 2, {};...
    'ROLL_AD_RATE', 2, {};...
    'AD7714Ch0Gain', 2, {};...
    'AH0_24V', 2, {};...
    'DEVICE5', 2, {};...
    'INTERNAL_PRESSURE', 2, {};...
    'VBD_BLEED_AD_RATE', 2, {};...
    'SM_CC', 2, {};...
    'D_PITCH', 2, {};...
    'SIM_W', 2, {};...
    'CALL_NDIVES', 2, {};...
    'D_FINISH', 2, {};...
    'ROLL_MAXERRORS', 2, {};...
    'UNCOM_BLEED', 2, {};...
    'ALTIM_TOP_TURN_MARGIN', 2, {};...
    'TT8_MAMPS', 2, {};...
    'PITCH_DBAND', 2, {};...
    'SEABIRD_C_I', 2, {};...
    'N_NOCOMM', 2, {};...
    'D_OFFGRID', 2, {};...
    'VBD_PUMP_AD_RATE_SURFACE', 2, {};...
    'RAFOS_HIT_WINDOW', 2, {};...
    'ALTIM_PULSE', 2, {};...
    'TCM_PITCH_OFFSET', 2, {};...
    'XPDR_PINGS', 2, {};...
    'D_GRID', 2, {};...
    'T_WATCHDOG', 2, {};...
    '_CALLS', 2, {};...
    'DEEPGLIDERMB', 2, {};...
    'T_RSLEEP', 2, {};...
    'XPDR_DEVICE', 2, {};...
    'ALTIM_FREQUENCY', 2, {};...
    'SURFACE_URGENCY_FORCE', 2, {};...
    'PITCH_MAXERRORS', 2, {};...
    'GLIDE_SLOPE', 2, {};...
    'GPS_DEVICE', 2, {};...
    'MOTHERBOARD', 2, {};...
    'MISSION', 2, {};...
    'ALTIM_BOTTOM_TURN_MARGIN', 2, {};...
    'KALMAN_Y', 2, {};...
    'SENSOR_SECS', 2, {};...
    'SENSORS', 2, {};...
    'DEVICES', 2, {};...
    'DATA_FILE_SIZE', 2, {};...
    'x10V_AH', 2, {};...
    'IRIDIUM_FIX', 2, {};...
    'FINISH', 2, {};...
    'FINISH1', 2, {};...
    'FINISH2', 2, {};...
    'CFSIZE', 2, {};...
    'MHEAD_RNG_PITCHd_Wd', 2, {};...
    'ALTIM_TOP_PING', 2, {};...
    'SM_CCo', 2, {};...
    'DEVICE_MAMPS', 2, {};...
    'SENSOR_MAMPS', 2, {};...
    'SM_PING', 2, {};...
    'KALMAN_CONTROL', 2, {};...
    'SM_GC', 2, {};...
    'ERRORS', 2, {};...
    'KALMAN_X', 2, {};...
    'x24V_AH', 2, {};...
    'RAFOS_FIX', 2, {};...
    'SPEED_LIMITS', 2, {};...
    'DEVICE_SECS', 2, {};...
    'TGT_LATLONG', 2, {};...
    'FREEZE', 2, {};...
    'STARTED', 2, {};...
    'ALTIM_BOTTOM_PING', 2, {};...
    'GPS1', 2, {'description','String reported in logfile for GPS1 fix (first surface position before dive)'};...
    'GPS2', 2, {'description','String reported in logfile for GPS2 fix (last surface position before dive)'};...
    'GPS', 2, {'description','String reported in logfile for GPS fix (first surface position after dive)'};...
    'gps_time', 2, {'standard_name','time'; 'units','seconds'; 'description','time in GMT epoch format'};...
    'gps_lat', 2, {'standard_name','lat'; 'units','decimal degrees'; 'description','gps fix latitude'};...
    'gps_lon', 2, {'standard_name','lon'; 'units','decimal degrees'; 'description','gps fix longitude'};...
    'STARTED', 2, {};...
    'ESCAPE_REASON', 2, {};...
    'SOUNDSPEED', 2, {};...
    'PITCH_ADJ_DBAND', 2, {};...
    'FERRY_MAX', 2, {};...
    'ROLL_ADJ_DBAND', 2, {};...
    'NAV_MODE', 2, {};...
    'ROLL_ADJ_GAIN', 2, {};...
    'PITCH_ADJ_GAIN', 2, {};...
    'ROLL_DEG', 2, {};...
    'KERMIT', 2, {};...
    'COMPASS_USE', 2, {};...
    'XPDR_INHIBIT', 2, {};...
    'COMPASS2_DEVICE', 2, {};...
    'PHONE_SUPPLY', 2, {};...
    'XPDR_VALID', 2, {};...
    'FG_AHR_10Vo', 2, {};...
    'FG_AHR_24Vo', 2, {};...
    'CT_XMITABOVE', 2, {};...
    'MINV_24V', 2, {};...
    'D_BOOST', 2, {};...
    'CT_PROFILE', 2, {};...
    'CT_RECORDABOVE', 2, {};...
    'FG_AHR_10V', 2, {};...
    'MEM', 2, {};...
    'MINV_10V', 2, {};...
    'FG_AHR_24V', 2, {};...
    'T_LOITER', 2, {};...
    'STROBE', 2, {};...
    'LOGGERS', 2, {};...
    'T_BOOST', 2, {};...
    };

DICT.sg_cal = {...
    'WETLabsCalData', 2, {};...
    'id_str', 2, {};...
    'mission_title', 2, {};...
    'calibcomm', 2, {};...
    'comm_oxy_type', 2, {};...
    'calibcomm_oxygen', 2, {};...
    'tol_glide', 2, {};...
    'optode_offset', 2, {};...
    'apply_tau_cond', 2, {};...
    'tautemp', 2, {};...
    'mass', 2, {};...
    'volmax', 2, {};...
    'rho0', 2, {};...
    'hd_a', 2, {};...
    'hd_b', 2, {};...
    'hd_c', 2, {};...
    'therm_expan', 2, {};...
    'temp_ref', 2, {};...
    'abs_compress', 2, {};...
    'pitchbias', 2, {};...
    'pitch_min_cnts', 2, {};...
    'pitch_max_cnts', 2, {};...
    'roll_min_cnts', 2, {};...
    'roll_max_cnts', 2, {};...
    'vbd_min_cnts', 2, {};...
    'vbd_max_cnts', 2, {};...
    'vbd_cnts_per_cc', 2, {};...
    'pump_rate_intercept', 2, {};...
    'pump_rate_slope', 2, {};...
    'pump_power_intercept', 2, {};...
    'pump_power_slope', 2, {};...
    't_g', 2, {};...
    't_h', 2, {};...
    't_i', 2, {};...
    't_j', 2, {};...
    'c_g', 2, {};...
    'c_h', 2, {};...
    'c_i', 2, {};...
    'c_j', 2, {};...
    'cpcor', 2, {};...
    'ctcor', 2, {};...
    'sbe_cond_freq_min', 2, {};...
    'sbe_cond_freq_max', 2, {};...
    'sbe_temp_freq_min', 2, {};...
    'sbe_temp_freq_max', 2, {};...
    'optode_FoilCoefA1', 2, {};...
    'optode_FoilCoefA2', 2, {};...
    'optode_FoilCoefA3', 2, {};...
    'optode_FoilCoefA4', 2, {};...
    'optode_FoilCoefA5', 2, {};...
    'optode_FoilCoefA6', 2, {};...
    'optode_FoilCoefA7', 2, {};...
    'optode_FoilCoefA8', 2, {};...
    'optode_FoilCoefA9', 2, {};...
    'optode_FoilCoefA10', 2, {};...
    'optode_FoilCoefA11', 2, {};...
    'optode_FoilCoefA12', 2, {};...
    'optode_FoilCoefA13', 2, {};...
    'optode_FoilCoefB1', 2, {};...
    'optode_FoilCoefB2', 2, {};...
    'optode_FoilCoefB3', 2, {};...
    'optode_FoilCoefB4', 2, {};...
    'optode_FoilCoefB5', 2, {};...
    'optode_FoilCoefB6', 2, {};...
    };


DICT.eng = {...
    'GC_phase', 2, {};...
    'aa4330_AirSat', 2, {};...
    'aa4330_CalPhase', 2, {};...
    'aa4330_O2', 2, {};...
    'aa4330_TCPhase', 2, {};...
    'aa4330_Temp', 2, {};...
    'basestation_version', 2, {};...
    'depth', 2, {};...
    'dive', 2, {};...
    'elaps_t', 2, {};...
    'elaps_t_0000', 2, {};...
    'fieldnames', 2, {};...
    'glider', 2, {};...
    'head', 2, {};...
    'mission', 2, {};...
    'pitchAng', 2, {};...
    'pitchCtl', 2, {};...
    'rec', 2, {};...
    'rollAng', 2, {};...
    'rollCtl', 2, {};...
    'sbect_condFreq', 2, {};...
    'sbect_tempFreq', 2, {};...
    'start', 2, {};...
    'vbdCC', 2, {};...
    'version', 2, {};...
    'wlbbfl2vmt_Cdomref', 2, {};...
    'wlbbfl2vmt_Cdomsig', 2, {};...
    'wlbbfl2vmt_Chlref', 2, {};...
    'wlbbfl2vmt_Chlsig', 2, {};...
    'wlbbfl2vmt_L2VMTtemp', 2, {};...
    'wlbbfl2vmt_wl600ref', 2, {};...
    'wlbbfl2vmt_wl600sig', 2, {};...
    };

DICT.flight = {};
end


function NC_CLASS = gt_sg_nc_vartype(DimLength,Type,Var)

if strcmp(class(Var),'double')
    if Type == 1 && numel(Var) == DimLength
        NC_CLASS = 'NC_DOUBLE';
    elseif numel(Var) == 1
        NC_CLASS = 'NC_DOUBLE';
    else
        NC_CLASS = 'NC_CHAR';
    end
else
    NC_CLASS = 'NC_CHAR';
end

end

function gt_sg_nc_struct2attr(ncid,varid,Var,Name)
fnames = fieldnames(Var);
if isstruct(Var)
    for istep = 1:numel(fnames)
        Nam
        gt_sg_nc_struct2attr(Var.(fnames{istep}),Name)
    end
else
    netcdf.putAtt(ncid,varid,'value',num2str(data.(file{vstep})(dstep).(Fields{fstep})));
end
end