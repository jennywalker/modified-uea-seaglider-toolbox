function [vi,XI,YI] = gt_sg_griddata_n(coords,v,coords_i,fun)
% function [vi,XI,YI] = gt_sg_griddata_n(coords,v,xi,yi,fun,interp,showplot)
%
% Rapid data gridding function with variable function (mean, median, var,
% etc.). Returns NaNs for empty grid cells in output but has the option to
% perform 1-D interpolation along a specified dimension (0, none; 1,
% vertical; 2, horizontal).
%
% Inputs:
% coords: cell array, with each cell containing coordinates in one
%       dimension.
% v: values of variable to be gridded.
% coords_i: cell array, with each cell containing an array describing one
%       of the gridded dimensions. Each cell must contain a monotonically
%       increasing array.
% fun: default is @mean; function to use on overlapping data in a grid
%       square. Other common functions are @median, @sum and @var.
%
% Outputs:
% vi: gridded values of v.
%
% By Bastien Y. Queste
% www.byqueste.com
% 15/07/2016
% ORV Sindhu Sadhana, BoBBLE cruise
%
%% START FUNCTION
% Parse input variables
if nargin < 6
    showplot = 0;
end
if nargin < 5
    interp = 0;
end
if nargin < 4
    fun = @mean;
end

dims = numel(coords);
v = v(:);

% Identify bin edges from the data
for dstep = 1:dims
    coords{dstep} = coords{dstep}(:);
    coords_i{dstep} = coords_i{dstep}(:);
    edges{dstep} = [coords_i{dstep}; 2*coords_i{dstep}(end) - coords_i{dstep}(end-1)] - (coords_i{dstep}(end) - coords_i{dstep}(end-1))/2;
end

% Identify non-NaN values
ind_valid = find(~isnan(sum([coords{:}],2)+v));

% Identify in which grid all data points will go into
for dstep = 1:dims
[~, bin{dstep}] = histc(coords{dstep}(ind_valid),...
    edges{dstep});
end

% Identify points outside of the grid
ind_inGrid = true(size(bin{1}));
for dstep = 1:dims
    ind_inGrid = ind_inGrid .* (bin{dstep} >= 1 & bin{dstep} <= numel(coords_i{dstep}));
end
ind_inGrid = find(ind_inGrid);

% Grid the data:
vi = accumarray(...
    [bin{:}],.... % Subscript indices indicating which bin the data is @fun'd into
    v(ind_valid(ind_inGrid)),... % Data to be gridded that are not NaN and in the grid.
    cellfun(@(x) numel(x),coords_i),... % Define size of the input matrix (in case there are missing columns or rowa at the end
    fun,... % Apply function, generally @mean, but @median or @var are common
    NaN); % Fill empty grid cells with NaNs rather than 0's
end